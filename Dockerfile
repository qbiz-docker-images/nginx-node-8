FROM nginx:latest

RUN apt-get update && apt-get install -y \
    curl \
    gnupg2  \
    git-core && \
    cd ~ && \
    curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh -y && \
    apt install -y \
    nodejs

WORKDIR /usr/share/nginx/html/app

EXPOSE 443 80
